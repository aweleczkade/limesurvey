FROM php:7-fpm-alpine
MAINTAINER Alexander Weleczka - https://aweleczka.de/

ENV LIME_FILE=limesurvey3.14.8+180829.tar.gz \
    LIME_CHECK=1725a1e09890c1ff4dee7afc1703478040c0b6ad7f25a60e15e16ca05053b675

WORKDIR /app
RUN set -ex \
    && apk --no-cache add postgresql-dev \
    && docker-php-ext-install mbstring pdo_pgsql
RUN set -ex \
    && echo "${LIME_CHECK} *limesurvey.tar.gz" > checksum.sha256 \
    && curl https://download.limesurvey.org/latest-stable-release/${LIME_FILE} -o limesurvey.tar.gz \
    && sha256sum -c checksum.sha256 \
    && tar -zxf limesurvey.tar.gz \
    && rm limesurvey.tar.gz